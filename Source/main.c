#include "PJ_RPI.h"
#include <stdio.h>
#include <time.h>
#include <mysql.h>

void finish_with_error(MYSQL *con)
{
	fprintf(stderr, "%s\n", mysql_error(con));
	mysql_close(con);
	exit(1);
}

int main()
{
	MYSQL *con  = mysql_init(NULL);
	printf("MySQL client version: %s\n", mysql_get_client_info());

	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		exit(1);
	}

	if (mysql_real_connect(con, "localhost", "webuser", "raspberry", "globe_bank", 0, NULL, 0) == NULL)
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		mysql_close(con);
		exit(1);

	}

	if(map_peripheral(&gpio) == -1)
	{
		printf("Failed to map the physical GPIO registers into the virtual memory space.\n");
		return -1;
	}

	// Define gpio as input
	INP_GPIO(23);
	INP_GPIO(24);

	// Define 
	int tmp23, tmp24;
	char sql_query[100];

	while(1)
	{
		tmp23 = GPIO_READ(23); // >> 23 bitwise 23 opschuiven RSHIFT omdat we een "groot getal" krijgen bij hoog, wanneer we dit echter binair omzetten zien we dit: 8388608 = 1000 0000 0000 0000 0000 0000 en na bitwise RSHIFT krijgen we 1;
		tmp24 = GPIO_READ(24); // >> 24; => 16777216 binair: 0001 0000 0000 0000 0000 0000 0000

		printf("value gpio %d: %d\n", 23, (tmp23 >> 23));
		printf("value gpio %d: %d\n", 24, (tmp24 >> 24));

		sprintf(sql_query,
			"INSERT INTO ioLogging(value, pin) VALUES (%d, %d), (%d, %d)",
			tmp23, 23, tmp24, 24
		);

		if (mysql_query(con, sql_query))
		{
			finish_with_error(con);
		}

		sleep(3); // Wait 3 seconds inbetween logging
	}

	mysql_close(con);
	return 0;
}