#include <gpiod.h>
#include <stdio.h>
#include <time.h>
#include <mysql.h>
#include <unistd.h>

#ifndef CONSUMER
#define CONSUMER "Consumer"
#endif

void finish_with_error(MYSQL *con)
{
	fprintf(stderr, "%s\n", mysql_error(con));
	mysql_close(con);
	exit(1);
}

int main()
{
	char *chipname = "gpiochip0";
	struct gpiod_chip *chip;

	MYSQL *con  = mysql_init(NULL);
	printf("MySQL client version: %s\n", mysql_get_client_info());

	if (con == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		exit(1);
	}

	if (mysql_real_connect(con, "localhost", "webuser", "raspberry", "globe_bank", 0, NULL, 0) == NULL)
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		mysql_close(con);
		exit(1);

	}

	chip = gpiod_chip_open_by_name(chipname);
	if (!chip)
	{
		return -1;
	}

	int tmp23, tmp24;
	char sql_query[100];
	struct gpiod_line *line23;
	struct gpiod_line *line24;
	int ret;

	// get line of gpiod that corresponds to pin 23
	line23 = gpiod_chip_get_line(chip, 23);
	// get line of gpiod that corresponds to pin 24
	line24 = gpiod_chip_get_line(chip, 24);

	if(!line23 || !line24)
	{
		return -1;
	}

	// pin as input
	ret = gpiod_line_request_input(line23, CONSUMER);
	if(ret < 0)
	{
		return -1;
	}

	ret = 0;

	// pin as input
	ret = gpiod_line_request_input(line24, CONSUMER);
	if(ret < 0)
	{
		return -1;
	}

	while(1)
	{
		// Reading pin 23 and 24 using their lines with gpiod
		tmp23 = gpiod_line_get_value(line23);
		tmp24 = gpiod_line_get_value(line24);

		printf("value gpio %d: %d\n", 23, (tmp23>>23));
		printf("value gpio %d: %d\n", 24, (tmp24>>24));

		sprintf(sql_query,
			"INSERT INTO ioLogging(value, pin) VALUES (%d, %d), (%d, %d)",
			tmp23, 23, tmp24, 24
		);

		if (mysql_query(con, sql_query))
		{
			finish_with_error(con);
		}

		sleep(3); // Wait 3 seconds inbetween logging
	}

	mysql_close(con);
	return 0;
}